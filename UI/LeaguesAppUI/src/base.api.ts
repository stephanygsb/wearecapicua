import { HttpHeaders } from '@angular/common/http';

export class BaseApi{
    endpoint = 'http://localhost:50303/api/';
    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };
    processResponse(response: Response) {
        const body = response;
        return body || { };
      }
}