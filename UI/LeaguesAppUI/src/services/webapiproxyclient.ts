import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class WebApiProxyClient{
    
    leagueApiEndpoint = 'http://localhost:50303/api/league/';
    teamApiEndpoint = 'http://localhost:50303/api/team/';
    playerApiEndpoint = 'http://localhost:50303/api/player/';
    
    constructor(private httpClient: HttpClient){ 
    }
    //Leagues
    getLeague(id):Observable<any>{
        return this.httpClient.get(this.leagueApiEndpoint + 'GetLeague/' + id).pipe(
            map(this.apiResponseProcess)
        );
    }
    
    getLeagues():Observable<any>{
        return this.httpClient.get(this.leagueApiEndpoint + 'GetLeagues').pipe(
            map(this.apiResponseProcess)
        );
    }
    
    private apiResponseProcess(response: Response) {
        const body = response;
        return body || { };
      }
    //Players
      getPlayer(id):Observable<any>{
        return this.httpClient.get(this.playerApiEndpoint + 'GetPlayer/' + id).pipe(
            map(this.apiResponseProcess)
        );
    }
    
    getPlayers():Observable<any>{
        return this.httpClient.get(this.playerApiEndpoint + 'GetPlayers').pipe(
            map(this.apiResponseProcess)
        );
    }

    getTeamPlayers(teamId):Observable<any>{
        return this.httpClient.get(this.playerApiEndpoint + 'GetTeamPlayers/' + teamId).pipe(
            map(this.apiResponseProcess)
        );
    }
    //Teams
    getTeam(teamId):Observable<any>{
        return this.httpClient.get(this.teamApiEndpoint + 'GetTeam/' + teamId).pipe(
            map(this.apiResponseProcess)
        );
    } 
    
    getTeams():Observable<any>{
        return this.httpClient.get(this.teamApiEndpoint + 'GetTeams').pipe(
            map(this.apiResponseProcess)
        );
    }

    getLeagueTeams(leagueId):Observable<any>{
        return this.httpClient.get(this.teamApiEndpoint + 'GetLeagueTeams/' + leagueId).pipe(
            map(this.apiResponseProcess)
        );
    }
}

