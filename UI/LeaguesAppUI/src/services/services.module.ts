import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { WebApiProxyClient } from './webapiproxyclient';


@NgModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        WebApiProxyClient
      ]
})
export class ServicesModule { }