import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { LeaguesComponent } from './leagues/leagues.component';


import { DatePipe } from '@angular/common';

import {

  MatFormFieldModule,

  MatAutocompleteModule,

  MatBadgeModule,

  MatBottomSheetModule,

  MatButtonModule,

  MatButtonToggleModule,

  MatCardModule,

  MatCheckboxModule,

  MatChipsModule,

  MatDatepickerModule,

  MatDialogModule,

  MatDividerModule,

  MatExpansionModule,

  MatGridListModule,

  MatIconModule,

  MatInputModule,

  MatListModule,

  MatMenuModule,

  MatNativeDateModule,

  MatPaginatorModule,

  MatProgressBarModule,

  MatProgressSpinnerModule,

  MatRadioModule,

  MatRippleModule,

  MatSelectModule,

  MatSidenavModule,

  MatSliderModule,

  MatSlideToggleModule,

  MatSnackBarModule,

  MatSortModule,

  MatStepperModule,

  MatTableModule,

  MatTabsModule,

  MatToolbarModule,

  MatTooltipModule,

  MatTreeModule,

} from '@angular/material';
import { ServicesModule } from 'src/services/services.module';



@NgModule({

  imports: [

    BrowserModule,

    AppRoutingModule,

    ServicesModule,

    BrowserAnimationsModule,

    MatFormFieldModule,

    MatAutocompleteModule,

    MatBadgeModule,

    MatBottomSheetModule,

    MatButtonModule,

    MatButtonToggleModule,

    MatCardModule,

    MatCheckboxModule,

    MatChipsModule,

    MatDatepickerModule,

    MatDialogModule,

    MatDividerModule,

    MatExpansionModule,

    MatGridListModule,

    MatIconModule,

    MatInputModule,

    MatListModule,

    MatMenuModule,

    MatNativeDateModule,

    MatPaginatorModule,

    MatProgressBarModule,

    MatProgressSpinnerModule,

    MatRadioModule,

    MatRippleModule,

    MatSelectModule,

    MatSidenavModule,

    MatSliderModule,

    MatSlideToggleModule,

    MatSnackBarModule,

    MatSortModule,

    MatStepperModule,

    MatTableModule,

    MatTabsModule,

    MatToolbarModule,

    MatTooltipModule,

    MatTreeModule,

  ],

  declarations: [

    AppComponent,

    LeaguesComponent

  ],

  exports: [

    DatePipe

  ],

  providers: [],

  bootstrap: [AppComponent]

})

export class AppModule { }
