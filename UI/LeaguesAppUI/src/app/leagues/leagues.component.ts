import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WebApiProxyClient } from 'src/services/webapiproxyclient';
import { MatSelectChange } from '@angular/material';

export interface ILeague {
  LeagueId:number; 
  Name:string; 
  FoundationDate:Date;
  Organizer:string;
}

export interface ITeam {
  TeamId:number;
  Name:string;
  Stadium:string;
}

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.scss']
})
export class LeaguesComponent implements OnInit {

  items:ILeague[];
  selectedLeague:ILeague;
  teams: ITeam[];
  selectedTeam:ITeam;

  constructor(private proxy: WebApiProxyClient) {}

  ngOnInit() {
    this.proxy.getLeagues().subscribe((data: ILeague[]) => {
      this.items = data;
    });
  }

  processSelectLeague(event: MatSelectChange){
    this.selectedLeague = this.items.find(league => league.LeagueId == event.value);
    this.findTeamsForLeague(this.selectedLeague.LeagueId);
  }

  findTeamsForLeague(leagueId: number): any {
    this.proxy.getLeagueTeams(leagueId).subscribe((data: ITeam[]) => {
      this.teams = data;
    });
  }

  processSelectTeam(event: MatSelectChange){
    this.selectedTeam = this.teams.find(team => team.TeamId == event.value);
  }

}
