﻿using System;
using System.Collections.Generic;
using Entities.BusinessEntities;

namespace DataAccess.Interfaces
{
    public interface IPlayerContext
    {
        List<Player> GetPlayers();
        Player GetPlayer(int id);
        List<Player> GetTeamPlayers(int teamId);
    }
}