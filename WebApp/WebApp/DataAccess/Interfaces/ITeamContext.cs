﻿using System;
using System.Collections.Generic;
using Entities.BusinessEntities;

namespace DataAccess.Interfaces
{
    public interface ITeamContext
    {
        List<Team> GetTeams();
        Team GetTeam(int id);
        List<Team> GetLeagueTeams(int leagueId);
    }
}