﻿using Entities.BusinessEntities;
using System;
using System.Collections.Generic;


namespace DataAccess.Interfaces
{
    public interface ILeagueContext
    {
        List<League> GetLeagues();
        League GetLeague(int id);
    }
}