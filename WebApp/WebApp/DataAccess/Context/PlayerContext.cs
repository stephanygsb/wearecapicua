﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.BusinessEntities;
using DataAccess.Interfaces;

namespace DataAccess.Context
{
    public class PlayerContext:IPlayerContext
    {
        public List<Player> GetPlayers()
        {
            List<Player> result = new List<Player>();
            using (var dbContext = new SystemContext())
                result = dbContext.Players.ToList();
            return result;
        }

        public Player GetPlayer( int id)
        {
            Player result = null;
            using (var dbContext = new SystemContext())
                result = dbContext.Players.Find(id);
            return result;
        }
        public List<Player> GetTeamPlayers(int teamId)
        {
            List<Player> result = new List<Player>();
            using (var dbContext = new SystemContext())
                result = dbContext.Players.Where(p => p.Team.TeamId == teamId).ToList();
            return result;
        }
    }
}