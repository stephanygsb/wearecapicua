﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.BusinessEntities;
using DataAccess.Interfaces;

namespace DataAccess.Context
{
    public class TeamContext:ITeamContext
    {
        public List<Team> GetTeams()
        {
            List<Team> result = new List<Team>();
            using (var dbContext = new SystemContext())
                result = dbContext.Teams.ToList();
            return result;
        }

        public Team GetTeam(int id)
        {
            Team result = null;
            using (var dbContext = new SystemContext())
                result = dbContext.Teams.Find(id);
            return result;
        }

        public List<Team> GetLeagueTeams(int leagueId)
        {
            List<Team> result = new List<Team>();
            using (var dbContext = new SystemContext())
                result = dbContext.Teams.Where(t => t.League.LeagueId == leagueId).ToList();
            return result;
        }

    }
}