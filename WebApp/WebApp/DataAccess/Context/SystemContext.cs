﻿using System.Data.Entity;
using Entities.BusinessEntities;

namespace DataAccess.Context
{
    public class SystemContext : DbContext
    {
        public DbSet<League> Leagues { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasMany(team => team.Players).WithOptional(player => player.Team);
            modelBuilder.Entity<League>().HasMany(league => league.Teams).WithRequired(team => team.League);
        }
    }
}