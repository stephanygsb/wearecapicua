﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.BusinessEntities;
using DataAccess.Interfaces;

namespace DataAccess.Context
{
    public class LeagueContext:ILeagueContext
    {
        public List<League> GetLeagues()
        {
            SetUpData();
            List<League> result = new List<League>();
            using (var dbContext = new SystemContext())
                result = dbContext.Leagues.ToList();
            return result;
        }

        public League GetLeague(int id)
        {
            League result = null;
            using (var dbContext = new SystemContext())
                result = dbContext.Leagues.Find(id);
            return result;
        }

        private void SetUpData()
        {
            List<League> leagues = new List<League>();
            leagues.Add(new League { LeagueId = 1, Name = "Uruguayo", FoundationDate = DateTime.Now, Organizer = "AUF" });
            leagues.Add(new League { LeagueId = 2, Name = "Argentino", FoundationDate = DateTime.Now, Organizer = "AAF" });
            leagues.Add(new League { LeagueId = 3, Name = "Colombia", FoundationDate = DateTime.Now, Organizer = "COL" });

            List<Team> teams = new List<Team>();
            teams.Add(new Team() { TeamId = 1, Name = "Peñarol", Stadium = "Campeón del siglo", League = leagues[0] });
            teams.Add(new Team() { TeamId = 2, Name = "Nacional", Stadium = "Parque central", League = leagues[0] });

            List<Player> players = new List<Player>();
            players.Add(new Player { PlayerId = 1, Age = 25, Name = "Lolo", LastName = "Estoyanof", Number = 11, Team = teams[0] });
            players.Add(new Player { PlayerId = 2, Age = 28, Name = "Christian", LastName = "Rodríguez", Number = 7, Team = teams[0] });
            players.Add(new Player { PlayerId = 3, Age = 20, Name = "Agustín", LastName = "Canobio", Number = 18, Team = teams[0] });

            using (var dbContext = new SystemContext())
            {
                dbContext.Leagues.AddRange(leagues);
                dbContext.SaveChanges();

                dbContext.Teams.AddRange(teams);
                dbContext.SaveChanges();

                dbContext.Players.AddRange(players);
                dbContext.SaveChanges();
            }
        }
    }
}