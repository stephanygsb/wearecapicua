﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.BusinessEntities
{
    public class League
    {
        [Key]
        public int LeagueId { get; set; }
        public string Name { get; set; }
        public DateTime FoundationDate { get; set; }
        public string Organizer { get; set; }
        public ICollection<Team> Teams { get; set; }
    }
}