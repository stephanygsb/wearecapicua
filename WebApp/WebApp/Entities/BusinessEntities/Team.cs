﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.BusinessEntities
{
    public class Team
    {
        [Key]
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string FoundationName { get; set; }
        public string Stadium { get; set; }
        public League League { get; set; }
        public ICollection<Player> Players { get; set; }
        
    }
}