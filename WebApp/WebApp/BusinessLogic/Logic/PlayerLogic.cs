﻿using System;
using System.Collections.Generic;
using Entities.BusinessEntities;
using DataAccess.Interfaces;
using DataAccess.Context;

namespace BusinessLogic.Logic
{
    public class PlayerLogic
    {
        IPlayerContext context = new PlayerContext();
        public List<Player> GetPlayers()
        {
            return context.GetPlayers();
        }
        public Player GetPlayer(int id)
        {
            return context.GetPlayer(id);
        }
        public List<Player> GetTeamPlayers(int teamId)
        {
            return context.GetTeamPlayers(teamId);
        }
    }
}