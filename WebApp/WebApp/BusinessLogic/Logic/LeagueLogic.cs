﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities.BusinessEntities;
using System.Configuration;
using DataAccess.Interfaces;
using DataAccess.Context;

namespace BusinessLogic.Logic
{
    public class LeagueLogic
    {
        ILeagueContext context = new LeagueContext();
        public List<League> GetLeagues()
        {
            return context.GetLeagues();
        }
        public League GetLeague(int id)
        {
            return context.GetLeague(id);
        }


    }
}