﻿using System;
using System.Collections.Generic;
using Entities.BusinessEntities;
using System.Configuration;
using DataAccess.Interfaces;
using DataAccess.Context;

namespace BusinessLogic.Logic
{
    public class TeamLogic
    {
        ITeamContext context = new TeamContext();
        public List<Team> GetTeams()
        {
            return context.GetTeams();
        }
        public Team GetTeam(int id)
        {
            return context.GetTeam(id);
        }

        public List<Team> GetLeagueTeams(int leagueId)
        {
            return context.GetLeagueTeams(leagueId);
        }

    }
}