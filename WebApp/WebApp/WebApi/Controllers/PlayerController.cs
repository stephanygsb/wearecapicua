﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using BusinessLogic.Logic;
using Entities.BusinessEntities;

namespace WebApp.Controllers
{
    public class PlayerController : ApiController
    {
        public IEnumerable<Player> GetPlayers()
        {
            return new PlayerLogic().GetPlayers();
        }

        public IHttpActionResult GetPlayer(int id)
        {
            var result = new PlayerLogic().GetPlayer(id);
            if (result != null)
                return Ok(result);
            else
                return NotFound();
        }
        [HttpGet]
        public IEnumerable<Player> GetTeamPlayers(int teamId)
        {
            return new PlayerLogic().GetTeamPlayers(teamId);
        }
    }
}