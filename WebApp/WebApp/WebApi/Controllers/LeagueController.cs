﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using BusinessLogic.Logic;
using Entities.BusinessEntities;

namespace WebApp.Controllers
{
    public class LeagueController : ApiController
    {
        public IEnumerable<League> GetLeagues()
        {
            return new LeagueLogic().GetLeagues();
        }

        
        public IHttpActionResult GetLeague(int id)
        {
            var result = new LeagueLogic().GetLeague(id);
            if (result != null)
                return Ok(result);
            else
                return NotFound();    
        }
        
        
    }
}
