﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using BusinessLogic.Logic;
using Entities.BusinessEntities;

namespace WebApp.Controllers
{
    public class TeamController : ApiController
    {
        public IEnumerable<Team> GetTeams()
        {
            return new TeamLogic().GetTeams();
        }

        public IHttpActionResult GetTeam(int id)
        {
            var result = new TeamLogic().GetTeam(id);
            if (result != null)
                return Ok(result);
            else
                return NotFound();
        }

        [HttpGet]
        public IEnumerable<Team> GetLeagueTeams(int id)
        {
            return new TeamLogic().GetLeagueTeams(id);
        }
    }
}